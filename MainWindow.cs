﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolSecretary.Enums;
using SchoolSecretary.Controllers;

using SchoolSecretary.Model;
using SchoolSecretary.Services;

namespace SchoolSecretary
{
	public partial class MainWindow : Form
	{
		private SchoolSecertaryDBContext _db;
		private EmployeeController _employeeController;

		private SubjectController _subjectController;
		private GroupController _groupController;
		private WorkTagController _workTagController;
		public MainWindow()
		{
			InitializeComponent();

			_db = new SchoolSecertaryDBContext();
			_employeeController = new EmployeeController(_db);
			_subjectController = new SubjectController(_db);
			_groupController = new GroupController(_db);
			_workTagController = new WorkTagController(_db);

			RefreshComboBoxes();

			RefreshDataGridViews();

		}


		void RefreshComboBoxes() 
		{
			InitGroupTypeComboBox();
			InitGroupFormComboBox();
			InitGroupLanguageComboBox();

			InitSubjectLanguageComboBox();
			InitSubjectToEComboBox();
			InitGroupSubjectComboBox();

			InitWorkTagSubjectComboBox();
			InitWorkTagTypeComboBox();
			InitWorkTagEmployeeComboBox();
			InitWorkTagGroupComboBox();
		}
		void RefreshDataGridViews() 
		{

			RefreshEmployeDataGridView();
			RefreshSubjectDataGridView();
			RefreshGroupDataGridView();
			RefreshWorkTagDataGridView();
		}
		void InitGroupTypeComboBox() 
		{
			Dictionary<GroupEnums.TypeOfStudy, string> typeValues = new Dictionary<GroupEnums.TypeOfStudy, string>();
			typeValues.Add(GroupEnums.TypeOfStudy.Bachelor, GroupEnums.ToSNames[(int)GroupEnums.TypeOfStudy.Bachelor]);
			typeValues.Add(GroupEnums.TypeOfStudy.Magister, GroupEnums.ToSNames[(int)GroupEnums.TypeOfStudy.Magister]);
			GroupTypeComboBox.DataSource = new BindingSource(typeValues, null);
			GroupTypeComboBox.DisplayMember = "Value";
			GroupTypeComboBox.ValueMember = "Key";
			GroupTypeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitGroupFormComboBox()
		{
			Dictionary<GroupEnums.FormOfStudy, string> formValues = new Dictionary<GroupEnums.FormOfStudy, string>();
			formValues.Add(GroupEnums.FormOfStudy.Present, GroupEnums.FoSNames[(int)GroupEnums.FormOfStudy.Present]);
			formValues.Add(GroupEnums.FormOfStudy.Combined, GroupEnums.FoSNames[(int)GroupEnums.FormOfStudy.Combined]);
			GroupFormComboBox.DataSource = new BindingSource(formValues, null);
			GroupFormComboBox.DisplayMember = "Value";
			GroupFormComboBox.ValueMember = "Key";
			GroupFormComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitGroupLanguageComboBox()
		{
			Dictionary<GroupEnums.Languages, string> languagesValues = new Dictionary<GroupEnums.Languages, string>();
			languagesValues.Add(GroupEnums.Languages.Czech, GroupEnums.LanguagesNames[(int)GroupEnums.Languages.Czech]);
			languagesValues.Add(GroupEnums.Languages.English, GroupEnums.LanguagesNames[(int)GroupEnums.Languages.English]);
			GroupLanguageComboBox.DataSource = new BindingSource(languagesValues, null);
			GroupLanguageComboBox.DisplayMember = "Value";
			GroupLanguageComboBox.ValueMember = "Key";
			GroupLanguageComboBox.DropDownStyle=ComboBoxStyle.DropDownList;
		}

		void InitSubjectLanguageComboBox()
		{
			Dictionary<GroupEnums.Languages, string> languagesValues = new Dictionary<GroupEnums.Languages, string>();
			languagesValues.Add(GroupEnums.Languages.Czech, GroupEnums.LanguagesNames[(int)GroupEnums.Languages.Czech]);
			languagesValues.Add(GroupEnums.Languages.English, GroupEnums.LanguagesNames[(int)GroupEnums.Languages.English]);
			SubjectLanguageComboBox.DataSource = new BindingSource(languagesValues, null);
			SubjectLanguageComboBox.DisplayMember = "Value";
			SubjectLanguageComboBox.ValueMember = "Key";
			SubjectLanguageComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitSubjectToEComboBox()
		{
			Dictionary<GroupEnums.TypeOfExam, string> toeValues = new Dictionary<GroupEnums.TypeOfExam, string>();
			toeValues.Add(GroupEnums.TypeOfExam.Exam, GroupEnums.TypeOfExamNames[(int)GroupEnums.TypeOfExam.Exam]);
			toeValues.Add(GroupEnums.TypeOfExam.Credit, GroupEnums.TypeOfExamNames[(int)GroupEnums.TypeOfExam.Credit]);
			toeValues.Add(GroupEnums.TypeOfExam.ClassifiedCredit, GroupEnums.TypeOfExamNames[(int)GroupEnums.TypeOfExam.ClassifiedCredit]);
			SubjectToEComboBox.DataSource = new BindingSource(toeValues, null);
			SubjectToEComboBox.DisplayMember = "Value";
			SubjectToEComboBox.ValueMember = "Key";
			SubjectToEComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitGroupSubjectComboBox()
		{
			Dictionary<Guid, string> subjectValues = new Dictionary<Guid, string>();
			var subjectList = _subjectController.GetList();
			int listBoxId = 0;
			foreach(var item in subjectList)
			{
				subjectValues.Add(item.ID, item.Code);
			}
			GroupSubjectComboBox.DataSource = new BindingSource(subjectValues, null);
			GroupSubjectComboBox.DisplayMember = "Value";
			GroupSubjectComboBox.ValueMember = "Key";

		}
		
		void InitWorkTagSubjectComboBox()
		{
			var list = _subjectController.GetList();
			if (list.Count > 0)
			{
				Dictionary<Guid, string> subjectValues = new Dictionary<Guid, string>();
				foreach (var item in list)
				{

					subjectValues.Add(item.ID, item.Code);
				}
			
			WorkTagSubjectComboBox.DataSource = new BindingSource(subjectValues, null);
			WorkTagSubjectComboBox.DisplayMember = "Value";
			WorkTagSubjectComboBox.ValueMember = "Key";
			}
			WorkTagSubjectComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitWorkTagGroupComboBox()
		{
			var list = _groupController.GetList();
			if (list.Count > 0)
			{
				Dictionary<Guid, string> groupValues = new Dictionary<Guid, string>();
				foreach (var item in list)
				{

					groupValues.Add(item.ID, item.Name);
				}

				WorkTagGroupComboBox.DataSource = new BindingSource(groupValues, null);
				WorkTagGroupComboBox.DisplayMember = "Value";
				WorkTagGroupComboBox.ValueMember = "Key";
			}
			WorkTagGroupComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitWorkTagTypeComboBox()
		{
			var list = GroupEnums.TypeOfWorkTagNames;

			Dictionary<GroupEnums.TypeOfWorkTag, string> typeValues = new Dictionary<GroupEnums.TypeOfWorkTag, string>();
			foreach (GroupEnums.TypeOfWorkTag item in GroupEnums.TypeOfWorkTag.GetValues(typeof(GroupEnums.TypeOfWorkTag)))
			{
				typeValues.Add(item, list[(int)item]);
			}
			WorkTagTypeComboBox.DataSource = new BindingSource(typeValues, null);
			WorkTagTypeComboBox.DisplayMember = "Value";
			WorkTagTypeComboBox.ValueMember = "Key";
			WorkTagTypeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}
		void InitWorkTagEmployeeComboBox()
		{
			var list = _employeeController.GetList();
			if (list.Count > 0)
			{
				Dictionary<Guid, string> employeeValues = new Dictionary<Guid, string>();
				foreach (var item in list)
				{

					employeeValues.Add(item.ID, item.Name);
				}
				WorkTagEmployeeComboBox.DataSource = new BindingSource(employeeValues, null);
				WorkTagEmployeeComboBox.DisplayMember = "Value";
				WorkTagEmployeeComboBox.ValueMember = "Key";
			}
			WorkTagEmployeeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
		}

		private void RefreshEmployeDataGridView() 
		{
			var list = _employeeController.GetView();
			EmployeeDataGridView.DataSource = list;

			EmployeeDataGridView.Columns["ID"].Visible = false;

			EmployeeDataGridView.Columns["WorkTags"].Visible = false;
			EmployeeDataGridView.Refresh();
			EmployeeDataGridView.CurrentCell = null;
		}

		private void RefreshSubjectDataGridView()
		{
			var list = _subjectController.GetView();

			SubjectDataGridView.DataSource = list;
			SubjectDataGridView.Columns["ID"].Visible = false;

			SubjectDataGridView.Columns["Groups"].Visible = false;

			SubjectDataGridView.Refresh();
			SubjectDataGridView.CurrentCell = null;
		}

		private void RefreshGroupDataGridView()
		{
			var list = _groupController.GetView();
			GroupDataGridView.DataSource = list;
			//GroupDataGridView.AutoGenerateColumns = false;;
			GroupDataGridView.Columns["ID"].Visible = false;

			GroupDataGridView.Refresh();
			GroupDataGridView.CurrentCell = null;
		}
		private void RefreshWorkTagDataGridView()
		{
			var list = _workTagController.GetView();
			WorkTagDataGridView.DataSource = list;
			WorkTagDataGridView.Columns["ID"].Visible = false;

			WorkTagDataGridView.Refresh();
			WorkTagDataGridView.CurrentCell = null;
		}
		private void EmployeeAddButton_Click(object sender, EventArgs e)
		{
			if (EmployeeNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			
			if (EmployeeEmailAdressTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter email of Employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				string email = EmployeeEmailAdressTextBox.Text;
				if(!email.Contains("@"))
				{
					MessageBox.Show("Please enter valid email of Employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					if (!email.Split('@')[1].Contains("."))
					{
						MessageBox.Show("Please enter real email of Employee", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					else
					{
						_employeeController.Add(EmployeeNameTextBox.Text, EmployeeEmailAdressTextBox.Text, EmployeeDoctorandCheckBox.Checked, (double)EmployeeWorkTimeNumeric.Value / 100);

						RefreshComboBoxes();
						RefreshDataGridViews();
					}
				}
				}
				
			
		}

		private void SubjectAddButton_Click(object sender, EventArgs e)
		{

			if (SubjectCodeTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter code of Subject", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			if (SubjectNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Subject", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else 
			{
				_subjectController
								.Add(
									SubjectNameTextBox.Text,
									SubjectCodeTextBox.Text,
									(int)SubjectCreditsNumericUpDown.Value,
									(int)SubjectWeeksNumericUpDown.Value,
									(int)SubjectLectureNumericUpDown.Value,
									(int)SubjectLessonsNumericUpDown.Value,
									(int)SubjectSeminarsNumericUpDown.Value,
									(int)SubjectToEComboBox.SelectedValue,
									(int)SubjectLanguageComboBox.SelectedValue,
									(int)SubjectStudentsCountNumericUpDown.Value
								);
				RefreshComboBoxes();
				RefreshDataGridViews();
			}
			
		}

		private void GroupAddButton_Click(object sender, EventArgs e)
		{
			if (GroupNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Group", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			if(GroupSubjectComboBox.SelectedItems.Count<=0)
			{
				MessageBox.Show( "Group must have some subjects", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				List<Subject> list = new List<Subject>();
				object[] array = new object[1];
				for (int i = 0; i < GroupSubjectComboBox.SelectedItems.Count; i++)
				{
					DataRowView valRow = (DataRowView)GroupSubjectComboBox.SelectedItems[i];
					string val = valRow[0].ToString();//guid
					string val1 = valRow[1].ToString();//valuevalRow[]
					list.Add(_subjectController.GetByID(new Guid(val)));
				}
				List<string> selectedItems = new List<string>();


				_groupController
					.Add(
						GroupNameTextBox.Text,
						(int)GroupClassYearNumeric.Value,
						(int)GroupFormComboBox.SelectedValue,
						(int)GroupTypeComboBox.SelectedValue,
						(int)GroupLanguageComboBox.SelectedValue,
						(int)GroupStudentsNumeric.Value,
						list
					);
				RefreshComboBoxes();
				RefreshDataGridViews();
			}
			
		}

		private void WorkTagAddButton_Click(object sender, EventArgs e)
		{

			if (WorkTagNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Worktag", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			
			else
			{
				if (WorkTagEmployeeComboBox.Text == "Nobody")
				{
					MessageBox.Show("You are assigning Worktag to Nobody, this is default non real person", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				var employee = _employeeController.GetByID(new Guid(WorkTagEmployeeComboBox.SelectedValue.ToString()));
				var subject = _subjectController.GetByID(new Guid(WorkTagSubjectComboBox.SelectedValue.ToString()));
				var group = WorkTagGroupComboBox.Text!=string.Empty ? _groupController.GetByID(new Guid(WorkTagGroupComboBox.SelectedValue.ToString())):null;
				_workTagController
					.Add(
						WorkTagNameTextBox.Text,
						subject,
						0,
						0,
						(int)WorkTagTypeComboBox.SelectedValue,
						subject.Language,
						(int)WorkTagStudentsNumericUpDown.Value,
						employee,
						group
					);

				RefreshComboBoxes();
				RefreshDataGridViews();
			}
			
		}

		private void GroupSubjectComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			

		}

		private void GroupDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}
		private void GroupDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex != -1)
			{
				Console.WriteLine(GroupDataGridView["ID", e.RowIndex].Value);
				var id = new Guid((GroupDataGridView["ID", e.RowIndex].Value.ToString()));
				var group = _groupController.GetByID(id);
				GroupNameTextBox.Text = group.Name;
				GroupClassYearNumeric.Value = group.ClassYear;
				GroupStudentsNumeric.Value = group.StudentsCount;
				GroupFormComboBox.SelectedValue = (GroupEnums.FormOfStudy)group.FormOfStudy;
				GroupTypeComboBox.SelectedValue = (GroupEnums.TypeOfStudy)group.TypeOfStudy;
				GroupLanguageComboBox.SelectedValue = (GroupEnums.Languages)group.Language;
				GroupSubjectComboBox.SelectedValue = group.Subjects;
			}
		}

		private void SubjectDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
		}

		private void EmployeeDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex == -1)
			{
				Console.WriteLine(EmployeeDataGridView["ID", e.RowIndex].Value);
			}
		}

		private void SubjectDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex == -1)
			{
				Console.WriteLine(SubjectDataGridView["ID", e.RowIndex].Value);
			}
		}

		private void WorkTagDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex != -1)
			{
				Console.WriteLine(WorkTagDataGridView["ID", e.RowIndex].Value);
				var id = new Guid((WorkTagDataGridView["ID", e.RowIndex].Value.ToString()));
				var workTag = _workTagController.GetByID(id);
				WorkTagNameTextBox.Text = workTag.Name;
				WorkTagSubjectComboBox.SelectedValue = workTag.Subject.ID;
				WorkTagStudentsNumericUpDown.Value = workTag.StudentCount;
				WorkTagTypeComboBox.SelectedValue =(GroupEnums.TypeOfWorkTag)workTag.TypeOfTag;
				WorkTagEmployeeComboBox.SelectedValue = workTag.EmployeeID;
			}	
		}

		private void WorkTagUpdateButton_Click(object sender, EventArgs e)
		{
			if (WorkTagDataGridView.CurrentCell == null)
			{
				MessageBox.Show("Please select valid cell to delete", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			if (WorkTagNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Worktag", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			
			else
			{
				if (WorkTagEmployeeComboBox.Text == "Nobody")
				{
					MessageBox.Show("You are assigning Worktag to Nobody, this is default non real person", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				_workTagController.Edit(new Guid(WorkTagDataGridView["ID", WorkTagDataGridView.CurrentCell.RowIndex].Value.ToString()), _employeeController.GetByID(new Guid(WorkTagEmployeeComboBox.SelectedValue.ToString())));
				RefreshDataGridViews();
			}
		}

		private void GroupEditButton_Click(object sender, EventArgs e)
		{
			if (GroupNameTextBox.Text == string.Empty)
			{
				MessageBox.Show("Please enter name of Group", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			//Console.WriteLine(GroupDataGridView["ID", GroupDataGridView.CurrentCell.RowIndex].Value.ToString());
			if (GroupDataGridView.CurrentCell==null)
			{
				MessageBox.Show("Please select valid cell to edit", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{

				_groupController.Edit(new Guid(GroupDataGridView["ID", GroupDataGridView.CurrentCell.RowIndex].Value.ToString()), (int)GroupStudentsNumeric.Value);
				RefreshDataGridViews();
			}
		}

		private void WorkTagDeleteButton_Click(object sender, EventArgs e)
		{
			if (WorkTagDataGridView.CurrentCell == null)
			{
				MessageBox.Show("Please select valid cell to delete", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				_workTagController.Delete(new Guid(WorkTagDataGridView["ID", WorkTagDataGridView.CurrentCell.RowIndex].Value.ToString()));
				RefreshDataGridViews();
			}
			
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			RefreshDataGridViews();
		}

		private void WorkTagDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			for (int i = 0; i < WorkTagDataGridView.Rows.Count; i++)
			{
				if ((int)WorkTagDataGridView["StudentCount", i].Value == 0)
				{
					WorkTagDataGridView.Rows[i].DefaultCellStyle.BackColor = Color.Red;
				}

			}
		}

		private void WorkTagExportButton_Click(object sender, EventArgs e)
		{
			if (WorkTagDataGridView.Rows.Count == 0)
			{
				MessageBox.Show("There is nothing to export", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else 
			{
				DataTable dataTable = DataTableConvertor.ToDataTable(WorkTagDataGridView);
				dataTable.TableName = "Worktags";

				using (var fbd = new FolderBrowserDialog())
				{
					DialogResult result = fbd.ShowDialog();

					if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
					{
						dataTable.WriteXml(fbd.SelectedPath + "\\Worktag_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xml");
					}
				}
			}
			
		}

		private void GroupDeleteButton_Click(object sender, EventArgs e)
		{
			if (GroupDataGridView.CurrentCell == null)
			{
				MessageBox.Show("Please select valid cell to delete", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				_groupController.Delete(new Guid(GroupDataGridView["ID", GroupDataGridView.CurrentCell.RowIndex].Value.ToString()));
				var employees = _employeeController.GetList();
				foreach (var item in employees)
				{
					_workTagController.UpdateEmployeePoints(item.ID);
				}
				RefreshDataGridViews();
			}
		}

		private void GroupExportButton_Click(object sender, EventArgs e)
		{
			if (GroupDataGridView.Rows.Count == 0)
			{
				MessageBox.Show("There is nothing to export", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				DataTable dataTable = DataTableConvertor.ToDataTable(GroupDataGridView);
				dataTable.TableName = "Groups";

				using (var fbd = new FolderBrowserDialog())
				{
					DialogResult result = fbd.ShowDialog();

					if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
					{
						dataTable.WriteXml(fbd.SelectedPath + "\\Groups_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xml");
					}
				}
			}
		}

		private void EmployeeExportButton_Click(object sender, EventArgs e)
		{
			if (EmployeeDataGridView.Rows.Count == 0)
			{
				MessageBox.Show("There is nothing to export", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				DataTable dataTable = DataTableConvertor.ToDataTable(EmployeeDataGridView);
				dataTable.TableName = "Employees";

				using (var fbd = new FolderBrowserDialog())
				{
					DialogResult result = fbd.ShowDialog();

					if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
					{
						dataTable.WriteXml(fbd.SelectedPath + "\\Employees_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xml");
					}
				}
			}
		}

		private void SubjectsExportButton_Click(object sender, EventArgs e)
		{
			if (SubjectDataGridView.Rows.Count == 0)
			{
				MessageBox.Show("There is nothing to export", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				DataTable dataTable = DataTableConvertor.ToDataTable(SubjectDataGridView);
				dataTable.TableName = "Subjects";

				using (var fbd = new FolderBrowserDialog())
				{
					DialogResult result = fbd.ShowDialog();

					if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
					{
						dataTable.WriteXml(fbd.SelectedPath + "\\Employees_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xml");
					}
				}
			}
		}
	}
}
