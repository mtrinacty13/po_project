Scénář:

    1) Vytvoření studijního oboru
    
        - umožnění CRUD operací
        
    2) Naplnění oboru předměty 
    
        - umožnění CRUD operací
        
    3) Vytvoření zaměstnance
    
        - umožnění CRUD operací
        
    4) Sestavení studijních událostí na základě oborů a jeho předmětů
    
    5) Sestavení úvazkového listu pro zaměstnance na základě studijních událostí
    
    
Otázky:

Odpovídá scénář  funkcionalitě aplikace ?

Co je studijní událost, nebo jak se řeší rozdělení studijních událostí(myšleno např. zda řešit nějaké překrývání cvičení, přednášek, místnosti, časy)


Technologie:

.Net

Winforms(pravděpodobně)