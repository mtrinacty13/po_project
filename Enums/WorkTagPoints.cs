﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Enums
{
	public static class WorkTagPoints
	{
		public static double Lecture = 1.8;
		public static double Lesson = 1.2;
		public static double Seminar = 1.2;
		public static double LectureEng = 2.4;
		public static double LessonEng = 1.8;
		public static double SeminarEng = 1.8;
		public static double Credit = 0.2;
		public static double ClassifiedCredit = 0.3;
		public static double Exam = 0.4;
		public static double CreditEng = 0.2;
		public static double ClassifiedCreditEng = 0.3;
		public static double ExamEng = 0.4;

	}
}
