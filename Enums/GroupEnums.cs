﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Enums
{
	public static class GroupEnums
	{
		public enum FormOfStudy 
		{
			Present,
			Combined
		}
		public static string[] FoSNames = { "Present", "Combined" };
		
		public enum TypeOfStudy 
		{
			Bachelor,
			Magister
		}
		public static string[] ToSNames = { "Bachelor", "Magister" };
		public enum Languages
		{
			Czech,
			English
		}
		public static string[] LanguagesNames = { "Czech", "English" };

		public enum TypeOfExam
		{
			Exam,
			Credit,
			ClassifiedCredit
		}
		public static string[] TypeOfExamNames = { "Exam", "Credit","Classified Credit" };

		public enum TypeOfWorkTag
		{
			Lecture,
			Lesson,
			Seminar,
			Exam,
			Credit,
			ClassifiedCredit
		}
		public static string[] TypeOfWorkTagNames = { "Lecture", "Lesson", "Seminar" , "Exam", "Credit", "Classified Credit" };



	}
}
