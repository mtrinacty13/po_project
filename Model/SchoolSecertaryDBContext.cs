﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SchoolSecretary.Model
{
	public class SchoolSecertaryDBContext : DbContext
	{
		public DbSet<Group> Groups { get; set; }
		public DbSet<Subject> Subjects { get; set; }
		public DbSet<Employee> Employees { get; set; }
		public DbSet<WorkTag> WorkTags { get; set; }
		//public DbSet<GroupSubject> GroupSubjects {get;set;}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
