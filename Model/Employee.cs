﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model
{
	public class Employee
	{
		
		public Guid ID { get; set; }
		public string Name { get; set; }
		public string EmailAdress { get; set; }
		public bool IsDoctorand { get; set; }
		public double Points { get; set; }
		public double PointsEng { get; set; }
		public double WorkTime { get; set; }

		public virtual ICollection<WorkTag> WorkTags { get; set; }

	}
}
