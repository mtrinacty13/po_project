﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model.ViewModel
{
	public class SubjectItem
	{
		public Guid ID { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public int Credits { get; set; }
		public int Weeks { get; set; }
		public int Lectures { get; set; }
		public int Lessons { get; set; }
		public int Seminars { get; set; }
		public string TypeOfExam { get; set; }
		public string Language { get; set; }
		public int StudentsCount { get; set; }

		public string Groups { get; set; }
	}
}
