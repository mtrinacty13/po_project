﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model.ViewModel
{
	public class EmployeeItem
	{
		public Guid ID { get; set; }
		public string Name { get; set; }
		public string EmailAdress { get; set; }
		public string IsDoctorand { get; set; }
		public double Points { get; set; }
		public double PointsEng { get; set; }
		public double WorkTime { get; set; }
		public string WorkTags { get; set; }
	}
}
