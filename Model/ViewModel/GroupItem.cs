﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model.ViewModel
{
	public class GroupItem
	{
		public Guid ID { get; set; }
		public string Name { get; set; }
		public int ClassYear { get; set; }
		public int StudentsCount { get; set; }
		public int FormOfStudy { get; set; }
		public int TypeOfStudy { get; set; }
		public int Language { get; set; }
		public string Subjects { get; set; }
	}
}
