﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model.ViewModel
{
	public class WorkTagItem
	{
		public Guid ID { get; set; }
		public string Employee { get; set; }
		public string Name { get; set; }
		public string Subject { get; set; }
		public string TypeOfTag { get; set; }
		public int StudentCount { get; set; }
		public int Hours { get; set; }
		public int Week { get; set; }
		public string Language { get; set; }
		public double Points { get; set; }
		public string Group { get; set; }
	}
}
