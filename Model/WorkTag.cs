﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Model
{
	public class WorkTag
	{
		public Guid ID { get; set; }
		public string Name { get; set; }
		public Subject Subject { get; set; }
		public int TypeOfTag { get; set; }
		public int StudentCount { get; set; }
		public int Hours { get; set; }
		public int Week { get; set; }
		public int Language { get; set; }
		public double Points { get; set; }
		public Guid EmployeeID { get; set; }
		public Employee Employee { get; set; }
		public Group Group { get; set; }
	}
}
