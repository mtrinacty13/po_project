﻿
namespace SchoolSecretary
{
	partial class MainWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.Employees = new System.Windows.Forms.TabControl();
			this.GroupsTab = new System.Windows.Forms.TabPage();
			this.GroupDataGridView = new System.Windows.Forms.DataGridView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.GroupExportButton = new System.Windows.Forms.Button();
			this.GroupDeleteButton = new System.Windows.Forms.Button();
			this.GroupEditButton = new System.Windows.Forms.Button();
			this.GroupSubjectComboBox = new Syncfusion.Windows.Forms.Tools.MultiSelectionComboBox();
			this.label26 = new System.Windows.Forms.Label();
			this.GroupStudentsNumeric = new System.Windows.Forms.NumericUpDown();
			this.GroupClassYearNumeric = new System.Windows.Forms.NumericUpDown();
			this.GroupAddButton = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.GroupFormComboBox = new System.Windows.Forms.ComboBox();
			this.GroupTypeComboBox = new System.Windows.Forms.ComboBox();
			this.GroupLanguageComboBox = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.GroupLabelName = new System.Windows.Forms.Label();
			this.GroupNameTextBox = new System.Windows.Forms.TextBox();
			this.EmployeesTab = new System.Windows.Forms.TabPage();
			this.EmployeeDataGridView = new System.Windows.Forms.DataGridView();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.EmployeeExportButton = new System.Windows.Forms.Button();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.EmployeeWorkTimeNumeric = new System.Windows.Forms.NumericUpDown();
			this.EmployeeAddButton = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this.EmployeeDoctorandCheckBox = new System.Windows.Forms.CheckBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.EmployeeEmailAdressTextBox = new System.Windows.Forms.TextBox();
			this.EmployeeNameTextBox = new System.Windows.Forms.TextBox();
			this.SubjectTab = new System.Windows.Forms.TabPage();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.SubjectsExportButton = new System.Windows.Forms.Button();
			this.SubjectDataGridView = new System.Windows.Forms.DataGridView();
			this.SubjectStudentsCountNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.SubjectLanguageComboBox = new System.Windows.Forms.ComboBox();
			this.SubjectToEComboBox = new System.Windows.Forms.ComboBox();
			this.SubjectSeminarsNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.SubjectLessonsNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.SubjectLectureNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.SubjectWeeksNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.SubjectCreditsNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label25 = new System.Windows.Forms.Label();
			this.SubjectAddButton = new System.Windows.Forms.Button();
			this.label24 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.SubjectNameTextBox = new System.Windows.Forms.TextBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.SubjectCodeTextBox = new System.Windows.Forms.TextBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.WorkTagDataGridView = new System.Windows.Forms.DataGridView();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.WorkTagExportButton = new System.Windows.Forms.Button();
			this.WorkTagDeleteButton = new System.Windows.Forms.Button();
			this.WorkTagUpdateButton = new System.Windows.Forms.Button();
			this.WorkTagEmployeeComboBox = new System.Windows.Forms.ComboBox();
			this.label16 = new System.Windows.Forms.Label();
			this.WorkTagAddButton = new System.Windows.Forms.Button();
			this.WorkTagStudentsNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label19 = new System.Windows.Forms.Label();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.WorkTagSubjectComboBox = new System.Windows.Forms.ComboBox();
			this.WorkTagTypeComboBox = new System.Windows.Forms.ComboBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.WorkTagNameTextBox = new System.Windows.Forms.TextBox();
			this.entityCommand1 = new System.Data.Entity.Core.EntityClient.EntityCommand();
			this.entityCommand2 = new System.Data.Entity.Core.EntityClient.EntityCommand();
			this.WorkTagGroupComboBox = new System.Windows.Forms.ComboBox();
			this.label20 = new System.Windows.Forms.Label();
			this.Employees.SuspendLayout();
			this.GroupsTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GroupDataGridView)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GroupSubjectComboBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GroupStudentsNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GroupClassYearNumeric)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.EmployeesTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.EmployeeDataGridView)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.EmployeeWorkTimeNumeric)).BeginInit();
			this.SubjectTab.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.SubjectDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectStudentsCountNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectSeminarsNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectLessonsNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectLectureNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectWeeksNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectCreditsNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.WorkTagDataGridView)).BeginInit();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.WorkTagStudentsNumericUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			this.SuspendLayout();
			// 
			// Employees
			// 
			this.Employees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Employees.Controls.Add(this.GroupsTab);
			this.Employees.Controls.Add(this.EmployeesTab);
			this.Employees.Controls.Add(this.SubjectTab);
			this.Employees.Controls.Add(this.tabPage3);
			this.Employees.Location = new System.Drawing.Point(12, 12);
			this.Employees.Name = "Employees";
			this.Employees.SelectedIndex = 0;
			this.Employees.Size = new System.Drawing.Size(895, 471);
			this.Employees.TabIndex = 0;
			// 
			// GroupsTab
			// 
			this.GroupsTab.Controls.Add(this.GroupDataGridView);
			this.GroupsTab.Controls.Add(this.groupBox1);
			this.GroupsTab.Location = new System.Drawing.Point(4, 22);
			this.GroupsTab.Name = "GroupsTab";
			this.GroupsTab.Padding = new System.Windows.Forms.Padding(3);
			this.GroupsTab.Size = new System.Drawing.Size(887, 445);
			this.GroupsTab.TabIndex = 0;
			this.GroupsTab.Text = "Groups";
			this.GroupsTab.UseVisualStyleBackColor = true;
			// 
			// GroupDataGridView
			// 
			this.GroupDataGridView.AllowUserToAddRows = false;
			this.GroupDataGridView.AllowUserToDeleteRows = false;
			this.GroupDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GroupDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.GroupDataGridView.Location = new System.Drawing.Point(7, 297);
			this.GroupDataGridView.Name = "GroupDataGridView";
			this.GroupDataGridView.ReadOnly = true;
			this.GroupDataGridView.Size = new System.Drawing.Size(874, 142);
			this.GroupDataGridView.TabIndex = 55;
			this.GroupDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GroupDataGridView_CellClick);
			this.GroupDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GroupDataGridView_CellContentClick);
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.GroupExportButton);
			this.groupBox1.Controls.Add(this.GroupDeleteButton);
			this.groupBox1.Controls.Add(this.GroupEditButton);
			this.groupBox1.Controls.Add(this.GroupSubjectComboBox);
			this.groupBox1.Controls.Add(this.label26);
			this.groupBox1.Controls.Add(this.GroupStudentsNumeric);
			this.groupBox1.Controls.Add(this.GroupClassYearNumeric);
			this.groupBox1.Controls.Add(this.GroupAddButton);
			this.groupBox1.Controls.Add(this.pictureBox1);
			this.groupBox1.Controls.Add(this.GroupFormComboBox);
			this.groupBox1.Controls.Add(this.GroupTypeComboBox);
			this.groupBox1.Controls.Add(this.GroupLanguageComboBox);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.GroupLabelName);
			this.groupBox1.Controls.Add(this.GroupNameTextBox);
			this.groupBox1.Location = new System.Drawing.Point(6, 6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(875, 285);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Group";
			// 
			// GroupExportButton
			// 
			this.GroupExportButton.Location = new System.Drawing.Point(473, 188);
			this.GroupExportButton.Name = "GroupExportButton";
			this.GroupExportButton.Size = new System.Drawing.Size(75, 21);
			this.GroupExportButton.TabIndex = 58;
			this.GroupExportButton.Text = "Export";
			this.GroupExportButton.UseVisualStyleBackColor = true;
			this.GroupExportButton.Click += new System.EventHandler(this.GroupExportButton_Click);
			// 
			// GroupDeleteButton
			// 
			this.GroupDeleteButton.Location = new System.Drawing.Point(392, 188);
			this.GroupDeleteButton.Name = "GroupDeleteButton";
			this.GroupDeleteButton.Size = new System.Drawing.Size(75, 21);
			this.GroupDeleteButton.TabIndex = 57;
			this.GroupDeleteButton.Text = "Delete";
			this.GroupDeleteButton.UseVisualStyleBackColor = true;
			this.GroupDeleteButton.Click += new System.EventHandler(this.GroupDeleteButton_Click);
			// 
			// GroupEditButton
			// 
			this.GroupEditButton.Location = new System.Drawing.Point(311, 188);
			this.GroupEditButton.Name = "GroupEditButton";
			this.GroupEditButton.Size = new System.Drawing.Size(75, 21);
			this.GroupEditButton.TabIndex = 56;
			this.GroupEditButton.Text = "Edit";
			this.GroupEditButton.UseVisualStyleBackColor = true;
			this.GroupEditButton.Click += new System.EventHandler(this.GroupEditButton_Click);
			// 
			// GroupSubjectComboBox
			// 
			this.GroupSubjectComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupSubjectComboBox.BeforeTouchSize = new System.Drawing.Size(131, 30);
			this.GroupSubjectComboBox.ButtonStyle = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
			this.GroupSubjectComboBox.DataSource = ((object)(resources.GetObject("GroupSubjectComboBox.DataSource")));
			this.GroupSubjectComboBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.GroupSubjectComboBox.Location = new System.Drawing.Point(93, 176);
			this.GroupSubjectComboBox.Name = "GroupSubjectComboBox";
			this.GroupSubjectComboBox.Size = new System.Drawing.Size(131, 30);
			this.GroupSubjectComboBox.TabIndex = 55;
			this.GroupSubjectComboBox.ThemeName = "Metro";
			this.GroupSubjectComboBox.UseVisualStyle = true;
			// 
			// label26
			// 
			this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(7, 177);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(43, 13);
			this.label26.TabIndex = 46;
			this.label26.Text = "Subject";
			// 
			// GroupStudentsNumeric
			// 
			this.GroupStudentsNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupStudentsNumeric.Location = new System.Drawing.Point(93, 71);
			this.GroupStudentsNumeric.Name = "GroupStudentsNumeric";
			this.GroupStudentsNumeric.ReadOnly = true;
			this.GroupStudentsNumeric.Size = new System.Drawing.Size(100, 20);
			this.GroupStudentsNumeric.TabIndex = 45;
			// 
			// GroupClassYearNumeric
			// 
			this.GroupClassYearNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupClassYearNumeric.Location = new System.Drawing.Point(93, 45);
			this.GroupClassYearNumeric.Name = "GroupClassYearNumeric";
			this.GroupClassYearNumeric.ReadOnly = true;
			this.GroupClassYearNumeric.Size = new System.Drawing.Size(100, 20);
			this.GroupClassYearNumeric.TabIndex = 44;
			// 
			// GroupAddButton
			// 
			this.GroupAddButton.Location = new System.Drawing.Point(230, 188);
			this.GroupAddButton.Name = "GroupAddButton";
			this.GroupAddButton.Size = new System.Drawing.Size(75, 21);
			this.GroupAddButton.TabIndex = 43;
			this.GroupAddButton.Text = "Add";
			this.GroupAddButton.UseVisualStyleBackColor = true;
			this.GroupAddButton.Click += new System.EventHandler(this.GroupAddButton_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.BackgroundImage = global::SchoolSecretary.Properties.Resources.utb_ikona;
			this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox1.Location = new System.Drawing.Point(639, 19);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(210, 190);
			this.pictureBox1.TabIndex = 15;
			this.pictureBox1.TabStop = false;
			// 
			// GroupFormComboBox
			// 
			this.GroupFormComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupFormComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.GroupFormComboBox.FormattingEnabled = true;
			this.GroupFormComboBox.Location = new System.Drawing.Point(93, 97);
			this.GroupFormComboBox.Name = "GroupFormComboBox";
			this.GroupFormComboBox.Size = new System.Drawing.Size(100, 21);
			this.GroupFormComboBox.TabIndex = 14;
			// 
			// GroupTypeComboBox
			// 
			this.GroupTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.GroupTypeComboBox.FormattingEnabled = true;
			this.GroupTypeComboBox.Location = new System.Drawing.Point(93, 122);
			this.GroupTypeComboBox.Name = "GroupTypeComboBox";
			this.GroupTypeComboBox.Size = new System.Drawing.Size(100, 21);
			this.GroupTypeComboBox.TabIndex = 13;
			// 
			// GroupLanguageComboBox
			// 
			this.GroupLanguageComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupLanguageComboBox.FormattingEnabled = true;
			this.GroupLanguageComboBox.Location = new System.Drawing.Point(93, 149);
			this.GroupLanguageComboBox.Name = "GroupLanguageComboBox";
			this.GroupLanguageComboBox.Size = new System.Drawing.Size(100, 21);
			this.GroupLanguageComboBox.TabIndex = 12;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 149);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 13);
			this.label5.TabIndex = 11;
			this.label5.Text = "Language";
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(7, 123);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(31, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Type";
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(7, 97);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(30, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Form";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 71);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Students";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 45);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Class Year";
			// 
			// GroupLabelName
			// 
			this.GroupLabelName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GroupLabelName.AutoSize = true;
			this.GroupLabelName.Location = new System.Drawing.Point(7, 19);
			this.GroupLabelName.Name = "GroupLabelName";
			this.GroupLabelName.Size = new System.Drawing.Size(35, 13);
			this.GroupLabelName.TabIndex = 4;
			this.GroupLabelName.Text = "Name";
			// 
			// GroupNameTextBox
			// 
			this.GroupNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.GroupNameTextBox.Location = new System.Drawing.Point(93, 19);
			this.GroupNameTextBox.Name = "GroupNameTextBox";
			this.GroupNameTextBox.Size = new System.Drawing.Size(100, 20);
			this.GroupNameTextBox.TabIndex = 0;
			// 
			// EmployeesTab
			// 
			this.EmployeesTab.Controls.Add(this.EmployeeDataGridView);
			this.EmployeesTab.Controls.Add(this.groupBox2);
			this.EmployeesTab.Location = new System.Drawing.Point(4, 22);
			this.EmployeesTab.Name = "EmployeesTab";
			this.EmployeesTab.Padding = new System.Windows.Forms.Padding(3);
			this.EmployeesTab.Size = new System.Drawing.Size(887, 445);
			this.EmployeesTab.TabIndex = 1;
			this.EmployeesTab.Text = "Employees";
			this.EmployeesTab.UseVisualStyleBackColor = true;
			// 
			// EmployeeDataGridView
			// 
			this.EmployeeDataGridView.AllowUserToAddRows = false;
			this.EmployeeDataGridView.AllowUserToDeleteRows = false;
			this.EmployeeDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.EmployeeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.EmployeeDataGridView.Location = new System.Drawing.Point(7, 250);
			this.EmployeeDataGridView.Name = "EmployeeDataGridView";
			this.EmployeeDataGridView.ReadOnly = true;
			this.EmployeeDataGridView.Size = new System.Drawing.Size(874, 150);
			this.EmployeeDataGridView.TabIndex = 1;
			this.EmployeeDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EmployeeDataGridView_CellClick);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.EmployeeExportButton);
			this.groupBox2.Controls.Add(this.pictureBox2);
			this.groupBox2.Controls.Add(this.EmployeeWorkTimeNumeric);
			this.groupBox2.Controls.Add(this.EmployeeAddButton);
			this.groupBox2.Controls.Add(this.label12);
			this.groupBox2.Controls.Add(this.EmployeeDoctorandCheckBox);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Controls.Add(this.EmployeeEmailAdressTextBox);
			this.groupBox2.Controls.Add(this.EmployeeNameTextBox);
			this.groupBox2.Location = new System.Drawing.Point(6, 6);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(875, 237);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Employee";
			// 
			// EmployeeExportButton
			// 
			this.EmployeeExportButton.Location = new System.Drawing.Point(174, 123);
			this.EmployeeExportButton.Name = "EmployeeExportButton";
			this.EmployeeExportButton.Size = new System.Drawing.Size(75, 21);
			this.EmployeeExportButton.TabIndex = 60;
			this.EmployeeExportButton.Text = "Export";
			this.EmployeeExportButton.UseVisualStyleBackColor = true;
			this.EmployeeExportButton.Click += new System.EventHandler(this.EmployeeExportButton_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox2.BackgroundImage = global::SchoolSecretary.Properties.Resources.utb_ikona;
			this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox2.Location = new System.Drawing.Point(639, 19);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(210, 193);
			this.pictureBox2.TabIndex = 44;
			this.pictureBox2.TabStop = false;
			// 
			// EmployeeWorkTimeNumeric
			// 
			this.EmployeeWorkTimeNumeric.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.EmployeeWorkTimeNumeric.Location = new System.Drawing.Point(93, 95);
			this.EmployeeWorkTimeNumeric.Name = "EmployeeWorkTimeNumeric";
			this.EmployeeWorkTimeNumeric.ReadOnly = true;
			this.EmployeeWorkTimeNumeric.Size = new System.Drawing.Size(100, 20);
			this.EmployeeWorkTimeNumeric.TabIndex = 43;
			// 
			// EmployeeAddButton
			// 
			this.EmployeeAddButton.Location = new System.Drawing.Point(93, 121);
			this.EmployeeAddButton.Name = "EmployeeAddButton";
			this.EmployeeAddButton.Size = new System.Drawing.Size(75, 23);
			this.EmployeeAddButton.TabIndex = 42;
			this.EmployeeAddButton.Text = "Add";
			this.EmployeeAddButton.UseVisualStyleBackColor = true;
			this.EmployeeAddButton.Click += new System.EventHandler(this.EmployeeAddButton_Click);
			// 
			// label12
			// 
			this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(7, 95);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(56, 13);
			this.label12.TabIndex = 41;
			this.label12.Text = "WorkTime";
			// 
			// EmployeeDoctorandCheckBox
			// 
			this.EmployeeDoctorandCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.EmployeeDoctorandCheckBox.AutoSize = true;
			this.EmployeeDoctorandCheckBox.Location = new System.Drawing.Point(93, 72);
			this.EmployeeDoctorandCheckBox.Name = "EmployeeDoctorandCheckBox";
			this.EmployeeDoctorandCheckBox.Size = new System.Drawing.Size(15, 14);
			this.EmployeeDoctorandCheckBox.TabIndex = 39;
			this.EmployeeDoctorandCheckBox.UseVisualStyleBackColor = true;
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(7, 71);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(57, 13);
			this.label9.TabIndex = 32;
			this.label9.Text = "Doctorand";
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(7, 45);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(67, 13);
			this.label10.TabIndex = 31;
			this.label10.Text = "Email Adress";
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(7, 19);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(35, 13);
			this.label11.TabIndex = 30;
			this.label11.Text = "Name";
			// 
			// EmployeeEmailAdressTextBox
			// 
			this.EmployeeEmailAdressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.EmployeeEmailAdressTextBox.Location = new System.Drawing.Point(93, 45);
			this.EmployeeEmailAdressTextBox.Name = "EmployeeEmailAdressTextBox";
			this.EmployeeEmailAdressTextBox.Size = new System.Drawing.Size(100, 20);
			this.EmployeeEmailAdressTextBox.TabIndex = 28;
			// 
			// EmployeeNameTextBox
			// 
			this.EmployeeNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.EmployeeNameTextBox.Location = new System.Drawing.Point(93, 19);
			this.EmployeeNameTextBox.Name = "EmployeeNameTextBox";
			this.EmployeeNameTextBox.Size = new System.Drawing.Size(100, 20);
			this.EmployeeNameTextBox.TabIndex = 27;
			// 
			// SubjectTab
			// 
			this.SubjectTab.Controls.Add(this.groupBox3);
			this.SubjectTab.Location = new System.Drawing.Point(4, 22);
			this.SubjectTab.Name = "SubjectTab";
			this.SubjectTab.Padding = new System.Windows.Forms.Padding(3);
			this.SubjectTab.Size = new System.Drawing.Size(887, 445);
			this.SubjectTab.TabIndex = 2;
			this.SubjectTab.Text = "Subjects";
			this.SubjectTab.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.SubjectsExportButton);
			this.groupBox3.Controls.Add(this.SubjectDataGridView);
			this.groupBox3.Controls.Add(this.SubjectStudentsCountNumericUpDown);
			this.groupBox3.Controls.Add(this.SubjectLanguageComboBox);
			this.groupBox3.Controls.Add(this.SubjectToEComboBox);
			this.groupBox3.Controls.Add(this.SubjectSeminarsNumericUpDown);
			this.groupBox3.Controls.Add(this.SubjectLessonsNumericUpDown);
			this.groupBox3.Controls.Add(this.SubjectLectureNumericUpDown);
			this.groupBox3.Controls.Add(this.SubjectWeeksNumericUpDown);
			this.groupBox3.Controls.Add(this.SubjectCreditsNumericUpDown);
			this.groupBox3.Controls.Add(this.label25);
			this.groupBox3.Controls.Add(this.SubjectAddButton);
			this.groupBox3.Controls.Add(this.label24);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.label23);
			this.groupBox3.Controls.Add(this.label22);
			this.groupBox3.Controls.Add(this.SubjectNameTextBox);
			this.groupBox3.Controls.Add(this.pictureBox3);
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.label15);
			this.groupBox3.Controls.Add(this.SubjectCodeTextBox);
			this.groupBox3.Location = new System.Drawing.Point(6, 6);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(875, 420);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Subjects";
			// 
			// SubjectsExportButton
			// 
			this.SubjectsExportButton.Location = new System.Drawing.Point(294, 255);
			this.SubjectsExportButton.Name = "SubjectsExportButton";
			this.SubjectsExportButton.Size = new System.Drawing.Size(75, 21);
			this.SubjectsExportButton.TabIndex = 61;
			this.SubjectsExportButton.Text = "Export";
			this.SubjectsExportButton.UseVisualStyleBackColor = true;
			this.SubjectsExportButton.Click += new System.EventHandler(this.SubjectsExportButton_Click);
			// 
			// SubjectDataGridView
			// 
			this.SubjectDataGridView.AllowUserToAddRows = false;
			this.SubjectDataGridView.AllowUserToDeleteRows = false;
			this.SubjectDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SubjectDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.SubjectDataGridView.Location = new System.Drawing.Point(0, 284);
			this.SubjectDataGridView.Name = "SubjectDataGridView";
			this.SubjectDataGridView.ReadOnly = true;
			this.SubjectDataGridView.Size = new System.Drawing.Size(874, 142);
			this.SubjectDataGridView.TabIndex = 54;
			this.SubjectDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SubjectDataGridView_CellClick);
			this.SubjectDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SubjectDataGridView_CellContentClick);
			// 
			// SubjectStudentsCountNumericUpDown
			// 
			this.SubjectStudentsCountNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectStudentsCountNumericUpDown.Location = new System.Drawing.Point(93, 258);
			this.SubjectStudentsCountNumericUpDown.Name = "SubjectStudentsCountNumericUpDown";
			this.SubjectStudentsCountNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectStudentsCountNumericUpDown.TabIndex = 53;
			this.SubjectStudentsCountNumericUpDown.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
			// 
			// SubjectLanguageComboBox
			// 
			this.SubjectLanguageComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectLanguageComboBox.FormattingEnabled = true;
			this.SubjectLanguageComboBox.Location = new System.Drawing.Point(93, 229);
			this.SubjectLanguageComboBox.Name = "SubjectLanguageComboBox";
			this.SubjectLanguageComboBox.Size = new System.Drawing.Size(100, 21);
			this.SubjectLanguageComboBox.TabIndex = 52;
			// 
			// SubjectToEComboBox
			// 
			this.SubjectToEComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectToEComboBox.FormattingEnabled = true;
			this.SubjectToEComboBox.Location = new System.Drawing.Point(93, 202);
			this.SubjectToEComboBox.Name = "SubjectToEComboBox";
			this.SubjectToEComboBox.Size = new System.Drawing.Size(100, 21);
			this.SubjectToEComboBox.TabIndex = 51;
			// 
			// SubjectSeminarsNumericUpDown
			// 
			this.SubjectSeminarsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectSeminarsNumericUpDown.Location = new System.Drawing.Point(93, 177);
			this.SubjectSeminarsNumericUpDown.Name = "SubjectSeminarsNumericUpDown";
			this.SubjectSeminarsNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectSeminarsNumericUpDown.TabIndex = 50;
			// 
			// SubjectLessonsNumericUpDown
			// 
			this.SubjectLessonsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectLessonsNumericUpDown.Location = new System.Drawing.Point(93, 151);
			this.SubjectLessonsNumericUpDown.Name = "SubjectLessonsNumericUpDown";
			this.SubjectLessonsNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectLessonsNumericUpDown.TabIndex = 49;
			// 
			// SubjectLectureNumericUpDown
			// 
			this.SubjectLectureNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectLectureNumericUpDown.Location = new System.Drawing.Point(93, 125);
			this.SubjectLectureNumericUpDown.Name = "SubjectLectureNumericUpDown";
			this.SubjectLectureNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectLectureNumericUpDown.TabIndex = 48;
			// 
			// SubjectWeeksNumericUpDown
			// 
			this.SubjectWeeksNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectWeeksNumericUpDown.Location = new System.Drawing.Point(93, 99);
			this.SubjectWeeksNumericUpDown.Name = "SubjectWeeksNumericUpDown";
			this.SubjectWeeksNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectWeeksNumericUpDown.TabIndex = 47;
			this.SubjectWeeksNumericUpDown.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
			// 
			// SubjectCreditsNumericUpDown
			// 
			this.SubjectCreditsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectCreditsNumericUpDown.Location = new System.Drawing.Point(93, 73);
			this.SubjectCreditsNumericUpDown.Name = "SubjectCreditsNumericUpDown";
			this.SubjectCreditsNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.SubjectCreditsNumericUpDown.TabIndex = 46;
			// 
			// label25
			// 
			this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(7, 151);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(46, 13);
			this.label25.TabIndex = 45;
			this.label25.Text = "Lessons";
			// 
			// SubjectAddButton
			// 
			this.SubjectAddButton.Location = new System.Drawing.Point(213, 255);
			this.SubjectAddButton.Name = "SubjectAddButton";
			this.SubjectAddButton.Size = new System.Drawing.Size(75, 23);
			this.SubjectAddButton.TabIndex = 43;
			this.SubjectAddButton.Text = "Add";
			this.SubjectAddButton.UseVisualStyleBackColor = true;
			this.SubjectAddButton.Click += new System.EventHandler(this.SubjectAddButton_Click);
			// 
			// label24
			// 
			this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(7, 260);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(80, 13);
			this.label24.TabIndex = 29;
			this.label24.Text = "Students Count";
			// 
			// label13
			// 
			this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(7, 229);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(55, 13);
			this.label13.TabIndex = 27;
			this.label13.Text = "Language";
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(7, 203);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(74, 13);
			this.label8.TabIndex = 25;
			this.label8.Text = "Type Of Exam";
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(7, 177);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(50, 13);
			this.label7.TabIndex = 23;
			this.label7.Text = "Seminars";
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(7, 125);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(48, 13);
			this.label6.TabIndex = 21;
			this.label6.Text = "Lectures";
			// 
			// label23
			// 
			this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(7, 99);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(41, 13);
			this.label23.TabIndex = 19;
			this.label23.Text = "Weeks";
			// 
			// label22
			// 
			this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(7, 47);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(35, 13);
			this.label22.TabIndex = 17;
			this.label22.Text = "Name";
			// 
			// SubjectNameTextBox
			// 
			this.SubjectNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectNameTextBox.Location = new System.Drawing.Point(93, 47);
			this.SubjectNameTextBox.Name = "SubjectNameTextBox";
			this.SubjectNameTextBox.Size = new System.Drawing.Size(100, 20);
			this.SubjectNameTextBox.TabIndex = 16;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox3.BackgroundImage = global::SchoolSecretary.Properties.Resources.utb_ikona;
			this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBox3.Location = new System.Drawing.Point(639, 19);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(210, 188);
			this.pictureBox3.TabIndex = 15;
			this.pictureBox3.TabStop = false;
			// 
			// label14
			// 
			this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(7, 73);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(39, 13);
			this.label14.TabIndex = 5;
			this.label14.Text = "Credits";
			// 
			// label15
			// 
			this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(7, 19);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(32, 13);
			this.label15.TabIndex = 4;
			this.label15.Text = "Code";
			// 
			// SubjectCodeTextBox
			// 
			this.SubjectCodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.SubjectCodeTextBox.Location = new System.Drawing.Point(93, 19);
			this.SubjectCodeTextBox.Name = "SubjectCodeTextBox";
			this.SubjectCodeTextBox.Size = new System.Drawing.Size(100, 20);
			this.SubjectCodeTextBox.TabIndex = 0;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.WorkTagDataGridView);
			this.tabPage3.Controls.Add(this.groupBox4);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(887, 445);
			this.tabPage3.TabIndex = 3;
			this.tabPage3.Text = "WorkTags";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// WorkTagDataGridView
			// 
			this.WorkTagDataGridView.AllowUserToAddRows = false;
			this.WorkTagDataGridView.AllowUserToDeleteRows = false;
			this.WorkTagDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.WorkTagDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.WorkTagDataGridView.Location = new System.Drawing.Point(6, 297);
			this.WorkTagDataGridView.Name = "WorkTagDataGridView";
			this.WorkTagDataGridView.ReadOnly = true;
			this.WorkTagDataGridView.Size = new System.Drawing.Size(874, 142);
			this.WorkTagDataGridView.TabIndex = 55;
			this.WorkTagDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.WorkTagDataGridView_CellClick);
			this.WorkTagDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.WorkTagDataGridView_CellFormatting);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.WorkTagGroupComboBox);
			this.groupBox4.Controls.Add(this.label20);
			this.groupBox4.Controls.Add(this.WorkTagExportButton);
			this.groupBox4.Controls.Add(this.WorkTagDeleteButton);
			this.groupBox4.Controls.Add(this.WorkTagUpdateButton);
			this.groupBox4.Controls.Add(this.WorkTagEmployeeComboBox);
			this.groupBox4.Controls.Add(this.label16);
			this.groupBox4.Controls.Add(this.WorkTagAddButton);
			this.groupBox4.Controls.Add(this.WorkTagStudentsNumericUpDown);
			this.groupBox4.Controls.Add(this.label19);
			this.groupBox4.Controls.Add(this.pictureBox4);
			this.groupBox4.Controls.Add(this.WorkTagSubjectComboBox);
			this.groupBox4.Controls.Add(this.WorkTagTypeComboBox);
			this.groupBox4.Controls.Add(this.label17);
			this.groupBox4.Controls.Add(this.label18);
			this.groupBox4.Controls.Add(this.label21);
			this.groupBox4.Controls.Add(this.WorkTagNameTextBox);
			this.groupBox4.Location = new System.Drawing.Point(6, 6);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(875, 240);
			this.groupBox4.TabIndex = 2;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Worktags";
			// 
			// WorkTagExportButton
			// 
			this.WorkTagExportButton.Location = new System.Drawing.Point(442, 158);
			this.WorkTagExportButton.Name = "WorkTagExportButton";
			this.WorkTagExportButton.Size = new System.Drawing.Size(75, 23);
			this.WorkTagExportButton.TabIndex = 61;
			this.WorkTagExportButton.Text = "Export";
			this.WorkTagExportButton.UseVisualStyleBackColor = true;
			this.WorkTagExportButton.Click += new System.EventHandler(this.WorkTagExportButton_Click);
			// 
			// WorkTagDeleteButton
			// 
			this.WorkTagDeleteButton.Location = new System.Drawing.Point(361, 158);
			this.WorkTagDeleteButton.Name = "WorkTagDeleteButton";
			this.WorkTagDeleteButton.Size = new System.Drawing.Size(75, 23);
			this.WorkTagDeleteButton.TabIndex = 60;
			this.WorkTagDeleteButton.Text = "Delete";
			this.WorkTagDeleteButton.UseVisualStyleBackColor = true;
			this.WorkTagDeleteButton.Click += new System.EventHandler(this.WorkTagDeleteButton_Click);
			// 
			// WorkTagUpdateButton
			// 
			this.WorkTagUpdateButton.Location = new System.Drawing.Point(280, 158);
			this.WorkTagUpdateButton.Name = "WorkTagUpdateButton";
			this.WorkTagUpdateButton.Size = new System.Drawing.Size(75, 23);
			this.WorkTagUpdateButton.TabIndex = 59;
			this.WorkTagUpdateButton.Text = "Edit";
			this.WorkTagUpdateButton.UseVisualStyleBackColor = true;
			this.WorkTagUpdateButton.Click += new System.EventHandler(this.WorkTagUpdateButton_Click);
			// 
			// WorkTagEmployeeComboBox
			// 
			this.WorkTagEmployeeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagEmployeeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.WorkTagEmployeeComboBox.FormattingEnabled = true;
			this.WorkTagEmployeeComboBox.Location = new System.Drawing.Point(93, 131);
			this.WorkTagEmployeeComboBox.Name = "WorkTagEmployeeComboBox";
			this.WorkTagEmployeeComboBox.Size = new System.Drawing.Size(100, 21);
			this.WorkTagEmployeeComboBox.TabIndex = 58;
			// 
			// label16
			// 
			this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(7, 132);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(53, 13);
			this.label16.TabIndex = 57;
			this.label16.Text = "Employee";
			// 
			// WorkTagAddButton
			// 
			this.WorkTagAddButton.Location = new System.Drawing.Point(199, 158);
			this.WorkTagAddButton.Name = "WorkTagAddButton";
			this.WorkTagAddButton.Size = new System.Drawing.Size(75, 23);
			this.WorkTagAddButton.TabIndex = 56;
			this.WorkTagAddButton.Text = "Add";
			this.WorkTagAddButton.UseVisualStyleBackColor = true;
			this.WorkTagAddButton.Click += new System.EventHandler(this.WorkTagAddButton_Click);
			// 
			// WorkTagStudentsNumericUpDown
			// 
			this.WorkTagStudentsNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagStudentsNumericUpDown.Location = new System.Drawing.Point(93, 75);
			this.WorkTagStudentsNumericUpDown.Name = "WorkTagStudentsNumericUpDown";
			this.WorkTagStudentsNumericUpDown.Size = new System.Drawing.Size(100, 20);
			this.WorkTagStudentsNumericUpDown.TabIndex = 55;
			// 
			// label19
			// 
			this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(7, 77);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(80, 13);
			this.label19.TabIndex = 54;
			this.label19.Text = "Students Count";
			// 
			// pictureBox4
			// 
			this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox4.BackgroundImage = global::SchoolSecretary.Properties.Resources.utb_ikona;
			this.pictureBox4.Location = new System.Drawing.Point(639, 19);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(210, 193);
			this.pictureBox4.TabIndex = 15;
			this.pictureBox4.TabStop = false;
			// 
			// WorkTagSubjectComboBox
			// 
			this.WorkTagSubjectComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagSubjectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.WorkTagSubjectComboBox.FormattingEnabled = true;
			this.WorkTagSubjectComboBox.Location = new System.Drawing.Point(93, 45);
			this.WorkTagSubjectComboBox.Name = "WorkTagSubjectComboBox";
			this.WorkTagSubjectComboBox.Size = new System.Drawing.Size(100, 21);
			this.WorkTagSubjectComboBox.TabIndex = 14;
			// 
			// WorkTagTypeComboBox
			// 
			this.WorkTagTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.WorkTagTypeComboBox.FormattingEnabled = true;
			this.WorkTagTypeComboBox.Location = new System.Drawing.Point(93, 104);
			this.WorkTagTypeComboBox.Name = "WorkTagTypeComboBox";
			this.WorkTagTypeComboBox.Size = new System.Drawing.Size(100, 21);
			this.WorkTagTypeComboBox.TabIndex = 13;
			// 
			// label17
			// 
			this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(7, 105);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(31, 13);
			this.label17.TabIndex = 9;
			this.label17.Text = "Type";
			// 
			// label18
			// 
			this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(7, 45);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(43, 13);
			this.label18.TabIndex = 7;
			this.label18.Text = "Subject";
			// 
			// label21
			// 
			this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(7, 19);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(35, 13);
			this.label21.TabIndex = 4;
			this.label21.Text = "Name";
			// 
			// WorkTagNameTextBox
			// 
			this.WorkTagNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagNameTextBox.Location = new System.Drawing.Point(93, 19);
			this.WorkTagNameTextBox.Name = "WorkTagNameTextBox";
			this.WorkTagNameTextBox.Size = new System.Drawing.Size(100, 20);
			this.WorkTagNameTextBox.TabIndex = 0;
			// 
			// entityCommand1
			// 
			this.entityCommand1.CommandTimeout = 0;
			this.entityCommand1.CommandTree = null;
			this.entityCommand1.Connection = null;
			this.entityCommand1.EnablePlanCaching = true;
			this.entityCommand1.Transaction = null;
			// 
			// entityCommand2
			// 
			this.entityCommand2.CommandTimeout = 0;
			this.entityCommand2.CommandTree = null;
			this.entityCommand2.Connection = null;
			this.entityCommand2.EnablePlanCaching = true;
			this.entityCommand2.Transaction = null;
			// 
			// WorkTagGroupComboBox
			// 
			this.WorkTagGroupComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.WorkTagGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.WorkTagGroupComboBox.FormattingEnabled = true;
			this.WorkTagGroupComboBox.Location = new System.Drawing.Point(93, 158);
			this.WorkTagGroupComboBox.Name = "WorkTagGroupComboBox";
			this.WorkTagGroupComboBox.Size = new System.Drawing.Size(100, 21);
			this.WorkTagGroupComboBox.TabIndex = 63;
			// 
			// label20
			// 
			this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(7, 159);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(36, 13);
			this.label20.TabIndex = 62;
			this.label20.Text = "Group";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(919, 495);
			this.Controls.Add(this.Employees);
			this.Name = "Form1";
			this.Text = "School Secretary tool";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.Employees.ResumeLayout(false);
			this.GroupsTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GroupDataGridView)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GroupSubjectComboBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GroupStudentsNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GroupClassYearNumeric)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.EmployeesTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.EmployeeDataGridView)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.EmployeeWorkTimeNumeric)).EndInit();
			this.SubjectTab.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.SubjectDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectStudentsCountNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectSeminarsNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectLessonsNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectLectureNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectWeeksNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SubjectCreditsNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			this.tabPage3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.WorkTagDataGridView)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.WorkTagStudentsNumericUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl Employees;
		private System.Windows.Forms.TabPage GroupsTab;
		private System.Windows.Forms.TabPage EmployeesTab;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox GroupNameTextBox;
		private System.Windows.Forms.Label GroupLabelName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox GroupFormComboBox;
		private System.Windows.Forms.ComboBox GroupTypeComboBox;
		private System.Windows.Forms.ComboBox GroupLanguageComboBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox EmployeeEmailAdressTextBox;
		private System.Windows.Forms.TextBox EmployeeNameTextBox;
		private System.Windows.Forms.CheckBox EmployeeDoctorandCheckBox;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Button EmployeeAddButton;
		private System.Windows.Forms.NumericUpDown EmployeeWorkTimeNumeric;
		private System.Windows.Forms.DataGridView EmployeeDataGridView;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.TabPage SubjectTab;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.PictureBox pictureBox3;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox SubjectCodeTextBox;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.PictureBox pictureBox4;
		private System.Windows.Forms.ComboBox WorkTagSubjectComboBox;
		private System.Windows.Forms.ComboBox WorkTagTypeComboBox;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox WorkTagNameTextBox;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox SubjectNameTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Button SubjectAddButton;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.NumericUpDown SubjectSeminarsNumericUpDown;
		private System.Windows.Forms.NumericUpDown SubjectLessonsNumericUpDown;
		private System.Windows.Forms.NumericUpDown SubjectLectureNumericUpDown;
		private System.Windows.Forms.NumericUpDown SubjectWeeksNumericUpDown;
		private System.Windows.Forms.NumericUpDown SubjectCreditsNumericUpDown;
		private System.Windows.Forms.NumericUpDown SubjectStudentsCountNumericUpDown;
		private System.Windows.Forms.ComboBox SubjectLanguageComboBox;
		private System.Windows.Forms.ComboBox SubjectToEComboBox;
		private System.Windows.Forms.DataGridView SubjectDataGridView;
		private System.Windows.Forms.DataGridView GroupDataGridView;
		private System.Windows.Forms.Button GroupAddButton;
		private System.Windows.Forms.NumericUpDown GroupStudentsNumeric;
		private System.Windows.Forms.NumericUpDown GroupClassYearNumeric;
		private System.Windows.Forms.Label label26;
		private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand1;
		private Syncfusion.Windows.Forms.Tools.MultiSelectionComboBox GroupSubjectComboBox;
		private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand2;
		private System.Windows.Forms.NumericUpDown WorkTagStudentsNumericUpDown;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button WorkTagAddButton;
		private System.Windows.Forms.DataGridView WorkTagDataGridView;
		private System.Windows.Forms.ComboBox WorkTagEmployeeComboBox;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Button WorkTagUpdateButton;
		private System.Windows.Forms.Button GroupEditButton;
		private System.Windows.Forms.Button WorkTagDeleteButton;
		private System.Windows.Forms.Button WorkTagExportButton;
		private System.Windows.Forms.Button GroupDeleteButton;
		private System.Windows.Forms.Button GroupExportButton;
		private System.Windows.Forms.Button EmployeeExportButton;
		private System.Windows.Forms.Button SubjectsExportButton;
		private System.Windows.Forms.ComboBox WorkTagGroupComboBox;
		private System.Windows.Forms.Label label20;
	}
}

