﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SchoolSecretary.Model;
using SchoolSecretary.Model.ViewModel;

namespace SchoolSecretary.Controllers
{
	public class EmployeeController
	{
		private SchoolSecertaryDBContext _db;
		public EmployeeController(SchoolSecertaryDBContext db)
		{
			_db = db;
		}
		public void Add(string name,string email,bool doctorand,double workTime)
		{
			var newEmployee = new Employee
			{
				ID = Guid.NewGuid(),
				Name= name,
				EmailAdress= email,
				IsDoctorand = doctorand,
				WorkTime = workTime
			};

			_db.Employees.Add(newEmployee);
			_db.SaveChanges();
		}
		public Employee GetByID(Guid id)
		{
			return _db.Employees.ToList().First(t => t.ID == id);
		}

		public List<Employee> GetList() 
		{
			return _db.Employees.ToList(); 
		}
		public List<EmployeeItem> GetView()
		{
			var list = _db.Employees.ToList();
			List<EmployeeItem> returnList = new List<EmployeeItem>();
			StringBuilder workTags = new StringBuilder("");
			foreach (var row in list)
			{
				if (row.WorkTags != null)
				{
					foreach (var item in row.WorkTags)
					{

						workTags.Append(item.Name + ";");
					}
				}
				var newRow = new EmployeeItem
				{
					ID = row.ID,
					Name = row.Name,
					EmailAdress = row.EmailAdress,
					IsDoctorand = row.IsDoctorand == true ? "Yes" : "No",
					Points = row.Points,
					PointsEng = row.PointsEng,
					WorkTags = workTags.ToString(),
					WorkTime = row.WorkTime
				};
				workTags.Clear();
				returnList.Add(newRow);
			}
			return returnList;
		}
		public void Edit(Guid id,Employee employeeEdit) 
		{
			var employee =_db.Employees.ToList().First(t => t.ID == id);
			employee = employeeEdit;
			_db.SaveChanges();
		}
	}
}
