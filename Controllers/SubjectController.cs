﻿using SchoolSecretary.Model;
using SchoolSecretary.Model.ViewModel;
using SchoolSecretary.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Controllers
{
	public class SubjectController
	{
		private SchoolSecertaryDBContext _db;
		public SubjectController(SchoolSecertaryDBContext db)
		{
			_db = db;
		}
		public void Add(string name, string code, int credits, int weeks, int lectures, int lessons, int seminars, int toe, int language, int studentsCount)
		{
			var newSubject = new Subject
			{
				ID = Guid.NewGuid(),
				Code=code,
				Name = name,
				Credits=credits,
				Weeks=weeks,
				Lectures=lectures,
				Lessons= lessons,
				Seminars= seminars,
				TypeOfExam=toe,
				Language=language,
				StudentsCount=studentsCount
			};
			
			_db.Subjects.Add(newSubject);
			_db.SaveChanges();
			

		}
		public List<Subject> GetList()
		{
			var list = _db.Subjects.ToList();
			return _db.Subjects.ToList();
		}
		public List<SubjectItem> GetView()
		{
			var list = _db.Subjects.ToList();
			List<SubjectItem> returnList = new List<SubjectItem>();
			StringBuilder groups = new StringBuilder();
			foreach (var row in list)
			{
				//List<string> subjects = new List<string>();
				foreach (var item in row.Groups)
				{

					groups.Append(item.Name + ";");
				}
				var newRow = new SubjectItem
				{
					ID = row.ID,
					Name = row.Name,
					Code = row.Code,
					Credits = row.Credits,
					Lectures = row.Lectures,
					Lessons = row.Lessons,
					Weeks = row.Weeks,
					Seminars = row.Seminars,
					TypeOfExam = GroupEnums.TypeOfExamNames[row.TypeOfExam],
					StudentsCount = row.StudentsCount,
					Language = GroupEnums.LanguagesNames[row.Language],
					Groups = groups.ToString()
				};
				groups.Clear();
				returnList.Add(newRow);
			}
			return returnList;
		}
		public Subject GetByID(Guid id)
		{
			return _db.Subjects.ToList().First(t => t.ID == id);
		}

		public void Edit(Guid id, int studentsCount)
		{
			var subject = _db.Subjects.ToList().Find(x => x.ID == id);
			subject.StudentsCount = studentsCount;
			_db.SaveChanges();

		}
		public void Delete(Guid id)
		{
			_db.Subjects.Remove(GetByID(id));
			_db.SaveChanges();
		}

	}
}
