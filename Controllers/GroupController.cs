﻿using SchoolSecretary.Enums;
using SchoolSecretary.Model;
using SchoolSecretary.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Controllers
{
	public class GroupController
	{
		private SchoolSecertaryDBContext _db;
		public GroupController(SchoolSecertaryDBContext db)
		{
			_db = db;
		}
		public void Add(string name,int classYear, int fos, int tos,int language, int studentsCount,List<Subject> list)
		{
			var newGroup = new Group
			{
				ID = Guid.NewGuid(),
				Name = name,
				ClassYear = classYear,
				StudentsCount = studentsCount,
				FormOfStudy = fos,
				TypeOfStudy=tos,
				Language = language,
				Subjects= list
			};

			_db.Groups.Add(newGroup);
			_db.SaveChanges();
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].Lectures != 0)
				{
					GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Lecture, studentsCount,list[i].Weeks,list[i].Lectures,newGroup);
				}

				if (list[i].Lessons != 0)
				{
					int cnt = (int)Math.Ceiling(((decimal)studentsCount / (decimal)list[i].StudentsCount));
					Console.WriteLine(cnt);
					for (int j = 0; j < cnt; j++)
					{
						
						int students =j==cnt-1 ? (studentsCount) % list[i].StudentsCount: list[i].StudentsCount ;
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Lesson,students, list[i].Weeks, list[i].Lessons, newGroup);
					}
					
				}

				if (list[i].Seminars != 0)
				{
					int cnt = (int)Math.Ceiling(((decimal)studentsCount / (decimal)list[i].StudentsCount));
					Console.WriteLine(cnt);
					for (int j = 0; j < cnt; j++)
					{

						int students = (j * list[i].StudentsCount) % list[i].StudentsCount == 0 ? list[i].StudentsCount : (j * list[i].StudentsCount) % list[i].StudentsCount;
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Lesson, students, list[i].Weeks, list[i].Seminars, newGroup);
					}
				}
				switch (list[i].TypeOfExam)
				{
					case (int)GroupEnums.TypeOfExam.Exam:
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Exam,studentsCount,0,0, newGroup);
						break;
					case (int)GroupEnums.TypeOfExam.Credit:
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Credit, studentsCount,0,0, newGroup);
						break;
					case (int)GroupEnums.TypeOfExam.ClassifiedCredit:
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.ClassifiedCredit, studentsCount,0,0, newGroup);
						break;
					default:
						break;
				}
			}
			

		}
		public List<Group> GetList()
		{
			return _db.Groups.ToList();
		}
		public List<GroupItem> GetView()
		{
			var list = _db.Groups.ToList();
			List<GroupItem> returnList = new List<GroupItem>();
			StringBuilder subjects = new StringBuilder();
			foreach (var row in list)
			{
				//List<string> subjects = new List<string>();
				foreach (var item in row.Subjects)
				{

					subjects.Append(item.Code +";");
				}
				var newRow = new GroupItem
				{
					ID = row.ID,
					Name = row.Name,
					ClassYear = row.ClassYear,
					StudentsCount = row.StudentsCount,
					FormOfStudy = row.FormOfStudy,
					TypeOfStudy = row.TypeOfStudy,
					Language = row.Language,
					Subjects = subjects.ToString()
				};
				subjects.Clear();
				returnList.Add(newRow);
			}
			return returnList;
		}
		public Group GetByID(Guid id) 
		{
			return _db.Groups.First(x => x.ID == id);
		}

		private void GenerateWorkTag(Subject subject, int tot, int studentsCount,int week, int hours,Group group)
		{
			WorkTagController workTagController = new WorkTagController(_db);
			workTagController
				.Add(
				subject.Code + " - " + GroupEnums.TypeOfWorkTagNames[tot],
				subject,
				week,
				hours,
				tot,
				subject.Language,
				studentsCount,
				null,
				group
				);

		}
		private void GenerateWorkTag(Subject subject, int tot, int studentsCount, int week, int hours, Group group,Employee emp)
		{
			WorkTagController workTagController = new WorkTagController(_db);
			workTagController
				.Add(
				subject.Code + " - " + GroupEnums.TypeOfWorkTagNames[tot],
				subject,
				week,
				hours,
				tot,
				subject.Language,
				studentsCount,
				emp,
				group
				);

		}
		

		private void ChangeWorkTagCount(Group group, int studentsCount)
		{
			List<int> studentsTagCount = new List<int>();
			List<WorkTag> workTags = group.WorkTags.ToList();
			WorkTagController workTagController = new WorkTagController(_db);
			group.StudentsCount = studentsCount;

			//List<string[]> tagWithEmp = new List<string[]>();
			Dictionary<Employee, string> tagWithEmp = new Dictionary<Employee, string>();

			Dictionary<Employee, string> tagWithEmpTMP = new Dictionary<Employee, string>();
			List<Subject> list = group.Subjects.ToList();
			int students = group.StudentsCount;
			foreach (var item in workTags)
			{
				if ((GroupEnums.TypeOfWorkTag)item.TypeOfTag == GroupEnums.TypeOfWorkTag.Lecture || (GroupEnums.TypeOfWorkTag)item.TypeOfTag == GroupEnums.TypeOfWorkTag.Exam || (GroupEnums.TypeOfWorkTag)item.TypeOfTag == GroupEnums.TypeOfWorkTag.ClassifiedCredit || (GroupEnums.TypeOfWorkTag)item.TypeOfTag == GroupEnums.TypeOfWorkTag.Credit)
				{
					item.StudentCount = studentsCount;
				}
				else
				{
					if (item.Employee.ID!= new Guid("0DFDACFF-1962-4219-A826-B5C0315B1513"))
					{
						//string[] newItem = { item.Employee.ID.ToString(),GroupEnums.TypeOfWorkTagNames[item.TypeOfTag]};
						tagWithEmp.Add(item.Employee, GroupEnums.TypeOfWorkTagNames[item.TypeOfTag]);
					}
					workTagController.Delete(item.ID);

				}
			}

			for (int i = 0; i < list.Count; i++)
			{

				if (list[i].Lessons != 0)
				{
					int cnt = (int)Math.Ceiling(((decimal)studentsCount / (decimal)list[i].StudentsCount));
					Console.WriteLine(cnt);
					for (int j = 0; j < cnt; j++)
					{
						Employee emp = null;
						foreach (var item in tagWithEmp)
						{
							if (item.Value==GroupEnums.TypeOfWorkTagNames[(int)GroupEnums.TypeOfWorkTag.Lesson])
							{
								emp = item.Key;
							}
						}
						
						
						int studentsLes = j == cnt - 1 ? (((studentsCount) % list[i].StudentsCount)== 0? list[i].StudentsCount: ((studentsCount) % list[i].StudentsCount)) : list[i].StudentsCount;

						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Lesson, studentsLes, list[i].Weeks, list[i].Lessons, group,emp);
						if (emp!=null)
						{
							tagWithEmp.Remove(emp);
						}
					}

				}

				if (list[i].Seminars != 0)
				{
					int cnt = (int)Math.Ceiling(((decimal)studentsCount / (decimal)list[i].StudentsCount));
					Console.WriteLine(cnt);
					for (int j = 0; j < cnt; j++)
					{
						Employee emp = null;
						foreach (var item in tagWithEmp)
						{
							if (item.Value == GroupEnums.TypeOfWorkTagNames[(int)GroupEnums.TypeOfWorkTag.Seminar])
							{
								emp = item.Key;
								tagWithEmp.Remove(item.Key);
							}
						}


						int studentsSem = j == cnt - 1 ? (((studentsCount) % list[i].StudentsCount) == 0 ? list[i].StudentsCount : ((studentsCount) % list[i].StudentsCount)) : list[i].StudentsCount;
						//int studentsSem = (j * list[i].StudentsCount) % list[i].StudentsCount == 0 ? list[i].StudentsCount : (j * list[i].StudentsCount) % list[i].StudentsCount;
						GenerateWorkTag(list[i], (int)GroupEnums.TypeOfWorkTag.Lesson, studentsSem, list[i].Weeks, list[i].Seminars, group,emp);
						if (emp != null)
						{
							tagWithEmp.Remove(emp);
						}
					}
				}
			}
			}

		public void Edit(Guid id, int studentsCount)
		{
			WorkTagController workTagController = new WorkTagController(_db);
			var group = _db.Groups.ToList().First(x => x.ID == id);
			ChangeWorkTagCount(group, studentsCount);
			foreach (var item in group.WorkTags)
			{
				workTagController.UpdateEmployeePoints(item.Employee.ID);
			}
			_db.SaveChanges();

		}
		public void Delete(Guid id)
		{
			foreach (var item in _db.WorkTags)
			{
				if (item.Group.ID==id)
				{
					_db.WorkTags.Remove(item);
				}
			}
			
			_db.Groups.Remove(GetByID(id));
			_db.SaveChanges();
		}
	}
}
