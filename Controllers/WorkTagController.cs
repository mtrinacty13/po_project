﻿using SchoolSecretary.Enums;
using SchoolSecretary.Model;
using SchoolSecretary.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolSecretary.Controllers
{
	public class WorkTagController
	{
		private SchoolSecertaryDBContext _db;
		public WorkTagController(SchoolSecertaryDBContext db)
		{
			_db = db;
		}

		public void Add(string name, Subject subject, int week,int hours,int tot, int language, int studentsCount, Employee employee,Group group)
		{
			if(employee==null)//if adding worktag, give us first employee in db, some dummy data
			{
				employee = _db.Employees.ToList().First(x=>x.ID== new Guid("0DFDACFF-1962-4219-A826-B5C0315B1513"));
			}
			
			var newWorktag = new WorkTag
			{
				ID = Guid.NewGuid(),
				Name = name,
				Language = language,
				StudentCount = studentsCount,
				Subject = subject,
				Hours =hours,
				TypeOfTag = tot,
				Week = week,
				Employee = employee,
				EmployeeID = employee.ID,
				Group =group
			};
			double points = CountPoints(newWorktag);
			newWorktag.Points = points;
			_db.WorkTags.Add(newWorktag);
			_db.SaveChanges();
		}
		public List<WorkTagItem> GetView()
		{
			var list = _db.WorkTags.ToList();
			
			List<WorkTagItem> returnList = new List<WorkTagItem>();
			foreach (var row in list)
			{
				//List<string> subjects = new List<string>();

				var newRow = new WorkTagItem
				{
					ID = row.ID,
					Name = row.Name,
					Language = GroupEnums.LanguagesNames[row.Language],
					Subject = row.Subject.Code,
					Employee = row.Employee.Name,
					Hours = row.Hours,
					Points = row.Points,
					StudentCount = row.StudentCount,
					TypeOfTag = GroupEnums.TypeOfWorkTagNames[row.TypeOfTag],
					Week = row.Week,
					Group = row.Group == null ? string.Empty : row.Group.Name
				};
				returnList.Add(newRow);
			}
			return returnList;
		}
		public WorkTag GetByID(Guid id)
		{
			return _db.WorkTags.ToList().Find(x => x.ID == id);
		}
		public void Edit(Guid id,Employee employee) 
		{
			var workTag = _db.WorkTags.ToList().Find(x => x.ID == id);
			workTag.Employee = employee;
			workTag.EmployeeID = employee.ID;
			_db.SaveChanges();
			UpdateEmployeePoints(employee.ID);
			 
		}
		public void Delete(Guid id) 
		{
			_db.WorkTags.Remove(GetByID(id));
			_db.SaveChanges();
		}
		public void UpdateEmployeePoints(Guid id)
		{
			EmployeeController employeeController = new EmployeeController(_db);
			double points = 0;
			double pointsEng = 0;
			var emp= employeeController.GetByID(id);
			foreach (var item in emp.WorkTags)
			{
				if((GroupEnums.Languages)item.Language==GroupEnums.Languages.Czech)
				{
					points += item.Points;
				}
				if ((GroupEnums.Languages)item.Language == GroupEnums.Languages.English)
				{
					pointsEng += item.Points;
				}
			}
			emp.Points = points;
			emp.PointsEng = pointsEng;
			employeeController.Edit(id, emp);

		}
		private double CountPoints(WorkTag workTag)
		{
			double points = 0;

			switch ((GroupEnums.TypeOfWorkTag)workTag.TypeOfTag)
			{
				case GroupEnums.TypeOfWorkTag.Lecture:
					if ((GroupEnums.Languages)workTag.Language == GroupEnums.Languages.Czech)
					{
						points = workTag.Week * WorkTagPoints.Lecture * workTag.Hours;
					}
					else
					{
						points = workTag.Week * WorkTagPoints.LectureEng * workTag.Hours;
					}
					break;
				case GroupEnums.TypeOfWorkTag.Lesson:
					if ((GroupEnums.Languages)workTag.Language == GroupEnums.Languages.Czech)
					{
						points = workTag.Week * WorkTagPoints.Lesson * workTag.Hours;
					}
					else
					{
						points = workTag.Week * WorkTagPoints.LessonEng * workTag.Hours;
					}
					break;
				case GroupEnums.TypeOfWorkTag.Seminar:
					if ((GroupEnums.Languages)workTag.Language == GroupEnums.Languages.Czech)
					{
						points = workTag.Week * WorkTagPoints.Seminar * workTag.Hours;
					}
					else
					{
						points = workTag.Week * WorkTagPoints.SeminarEng * workTag.Hours;
					}
					break;
				case GroupEnums.TypeOfWorkTag.Exam:
						points =WorkTagPoints.Exam * workTag.StudentCount;
					break;
				case GroupEnums.TypeOfWorkTag.Credit:
						points = WorkTagPoints.Credit * workTag.StudentCount;
					break;
				case GroupEnums.TypeOfWorkTag.ClassifiedCredit:
						points = WorkTagPoints.ClassifiedCredit * workTag.StudentCount;
					break;
			}
			return points;
		}
	}
}
