﻿namespace SchoolSecretary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupTagconnection : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.GroupSubjects", newName: "SubjectGroups");
            DropPrimaryKey("dbo.SubjectGroups");
            AddColumn("dbo.WorkTags", "Group_ID", c => c.Guid());
            AddPrimaryKey("dbo.SubjectGroups", new[] { "Subject_ID", "Group_ID" });
            CreateIndex("dbo.WorkTags", "Group_ID");
            AddForeignKey("dbo.WorkTags", "Group_ID", "dbo.Groups", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkTags", "Group_ID", "dbo.Groups");
            DropIndex("dbo.WorkTags", new[] { "Group_ID" });
            DropPrimaryKey("dbo.SubjectGroups");
            DropColumn("dbo.WorkTags", "Group_ID");
            AddPrimaryKey("dbo.SubjectGroups", new[] { "Group_ID", "Subject_ID" });
            RenameTable(name: "dbo.SubjectGroups", newName: "GroupSubjects");
        }
    }
}
