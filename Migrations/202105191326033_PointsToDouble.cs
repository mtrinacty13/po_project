﻿namespace SchoolSecretary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PointsToDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "Points", c => c.Double(nullable: false));
            AlterColumn("dbo.Employees", "PointsEng", c => c.Double(nullable: false));
            AlterColumn("dbo.WorkTags", "Points", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.WorkTags", "Points", c => c.Int(nullable: false));
            AlterColumn("dbo.Employees", "PointsEng", c => c.Int(nullable: false));
            AlterColumn("dbo.Employees", "Points", c => c.Int(nullable: false));
        }
    }
}
