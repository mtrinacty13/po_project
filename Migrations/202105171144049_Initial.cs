﻿namespace SchoolSecretary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        EmailAdress = c.String(),
                        IsDoctorand = c.Boolean(nullable: false),
                        Points = c.Int(nullable: false),
                        PointsEng = c.Int(nullable: false),
                        WorkTime = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkTags",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        TypeOfTag = c.Int(nullable: false),
                        StudentCount = c.Int(nullable: false),
                        Hours = c.Int(nullable: false),
                        Week = c.Int(nullable: false),
                        Language = c.Int(nullable: false),
                        Points = c.Int(nullable: false),
                        Employee_ID = c.Guid(),
                        Subject_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employees", t => t.Employee_ID)
                .ForeignKey("dbo.Subjects", t => t.Subject_ID)
                .Index(t => t.Employee_ID)
                .Index(t => t.Subject_ID);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Code = c.String(),
                        Name = c.String(),
                        Credits = c.Int(nullable: false),
                        Weeks = c.Int(nullable: false),
                        Lectures = c.Int(nullable: false),
                        Lessons = c.Int(nullable: false),
                        Seminars = c.Int(nullable: false),
                        TypeOfExam = c.Int(nullable: false),
                        Language = c.Int(nullable: false),
                        StudentsCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        ClassYear = c.Int(nullable: false),
                        StudentsCount = c.Int(nullable: false),
                        FormOfStudy = c.Int(nullable: false),
                        TypeOfStudy = c.Int(nullable: false),
                        Language = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GroupSubjects",
                c => new
                    {
                        Group_ID = c.Guid(nullable: false),
                        Subject_ID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Group_ID, t.Subject_ID })
                .ForeignKey("dbo.Groups", t => t.Group_ID, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.Subject_ID, cascadeDelete: true)
                .Index(t => t.Group_ID)
                .Index(t => t.Subject_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkTags", "Subject_ID", "dbo.Subjects");
            DropForeignKey("dbo.GroupSubjects", "Subject_ID", "dbo.Subjects");
            DropForeignKey("dbo.GroupSubjects", "Group_ID", "dbo.Groups");
            DropForeignKey("dbo.WorkTags", "Employee_ID", "dbo.Employees");
            DropIndex("dbo.GroupSubjects", new[] { "Subject_ID" });
            DropIndex("dbo.GroupSubjects", new[] { "Group_ID" });
            DropIndex("dbo.WorkTags", new[] { "Subject_ID" });
            DropIndex("dbo.WorkTags", new[] { "Employee_ID" });
            DropTable("dbo.GroupSubjects");
            DropTable("dbo.Groups");
            DropTable("dbo.Subjects");
            DropTable("dbo.WorkTags");
            DropTable("dbo.Employees");
        }
    }
}
