﻿namespace SchoolSecretary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmployeesWorktagsUpdateID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkTags", "Employee_ID", "dbo.Employees");
            DropIndex("dbo.WorkTags", new[] { "Employee_ID" });
            RenameColumn(table: "dbo.WorkTags", name: "Employee_ID", newName: "EmployeeID");
            AlterColumn("dbo.WorkTags", "EmployeeID", c => c.Guid(nullable: false));
            CreateIndex("dbo.WorkTags", "EmployeeID");
            AddForeignKey("dbo.WorkTags", "EmployeeID", "dbo.Employees", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkTags", "EmployeeID", "dbo.Employees");
            DropIndex("dbo.WorkTags", new[] { "EmployeeID" });
            AlterColumn("dbo.WorkTags", "EmployeeID", c => c.Guid());
            RenameColumn(table: "dbo.WorkTags", name: "EmployeeID", newName: "Employee_ID");
            CreateIndex("dbo.WorkTags", "Employee_ID");
            AddForeignKey("dbo.WorkTags", "Employee_ID", "dbo.Employees", "ID");
        }
    }
}
